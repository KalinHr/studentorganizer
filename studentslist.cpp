#include "studentslist.h"
#include "studentslistmodel.h"
#include "student.h"

#include <QQuickView>
#include <QQmlContext>

StudentsList::StudentsList()
    : _model(new StudentsListModel)
{
    this->setResizeMode(QQuickView::SizeRootObjectToView);

    QQmlContext *ctxt = this->rootContext();
    ctxt->setContextProperty("myModel", QVariant::fromValue(_model));

    this->setSource(QUrl("qrc:/studentsGridView.qml"));
}

void StudentsList::addStudent(Student *student)
{
    _model->addStudent(student);
}

void StudentsList::open(QList<Student *> students)
{
    _model->open(students);
}
