#include "student.h"

Student::Student(const QString& avatar,
                 const QString& firstName,
                 const QString& surName,
                 const QString& lastName,
                 const QString& fN,
                 const int& course)
{
    m_avatar = avatar;
    m_firstName = firstName;
    m_surName = surName;
    m_lastName = lastName;
    m_fN = fN;
    m_course = course;
}

QVariant Student::GetData(int column) const
{
    switch(column)
    {
        case 0:
            return m_avatar;
        case 1:
            return m_firstName;
        case 2:
            return m_surName;
        case 3:
            return m_lastName;
        case 4:
            return m_fN;
        case 5:
            return m_course;
        default:
            return QVariant();
    }
}

QString Student::avatar()
{
    return m_avatar;
}

QString Student::firstName()
{
    return m_firstName;
}

QString Student::surName()
{
    return m_surName;
}

QString Student::lastName()
{
    return m_lastName;
}

QString Student::fN()
{
    return m_fN;
}

int Student::course()
{
    return m_course;
}
