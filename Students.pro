QT += core gui widgets qml quick
SOURCES += \
    main.cpp \
    student.cpp \
    studentstablemodel.cpp \
    filecontroller.cpp \
    addstudentdialog.cpp \
    studentstable.cpp \
    mainwindow.cpp \
    studentslist.cpp \
    studentslistmodel.cpp

HEADERS += \
    student.h \
    studentstablemodel.h \
    filecontroller.h \
    addstudentdialog.h \
    studentstable.h \
    mainwindow.h \
    studentslist.h \
    studentslistmodel.h

RESOURCES += \
    files.qrc \
    qml.qrc
