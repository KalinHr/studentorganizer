#ifndef STUDENTSLIST_H
#define STUDENTSLIST_H

#include <studentslistmodel.h>

#include <QQuickView>

class StudentsList : public QQuickView
{
public:
    StudentsList();

    void addStudent(Student* student);
    void open(QList<Student*> students);

private:
    StudentsListModel* _model;
};

#endif // STUDENTSLIST_H
