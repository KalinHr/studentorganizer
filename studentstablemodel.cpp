#include "studentstablemodel.h"
#include "student.h"
#include "filecontroller.h"

#include <QIcon>
#include <QIODevice>
#include <QPixmap>

StudentsTableModel::StudentsTableModel(QObject *parent)
    :QAbstractTableModel(parent)
{
}

int StudentsTableModel::rowCount(const QModelIndex & /*parent*/) const
{
    return m_listOfStudents.count();
}

int StudentsTableModel::columnCount(const QModelIndex & /*parent*/) const
{
    return ColumnIndex::ColumnSize;
}

QVariant StudentsTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_listOfStudents.size())
        return QVariant();

    if (index.column() == 0)
    {
        if (role == Qt::DecorationRole) {
            QIcon icon(m_listOfStudents.at(index.row())->GetData(index.column()).toString());
            return icon;
        }
        else if (role == Qt::DisplayRole) {
            return "";
        }
    }

    if (role == Qt::DisplayRole)
    {
        return m_listOfStudents.at(index.row())->GetData(index.column());
    }

    return QVariant();
}

QVariant StudentsTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
         switch(section)
         {
             case 0:
                 return QString("Avatar");
             case 1:
                 return QString("First name");
             case 2:
                 return QString("Surname");
             case 3:
                 return QString("Last name");
             case 4:
                 return QString("FN");
             case 5:
                 return QString("Course");
        }
    }

    return QVariant();
}

void StudentsTableModel::addStudent(Student* const aStudent)
{
    beginInsertRows( QModelIndex() , rowCount(), rowCount() );
    m_listOfStudents.push_back(aStudent);
    endInsertRows();
}

void StudentsTableModel::addStudents( const QList< Student* >& aStudents )
{
    foreach(Student* student, aStudents)
    {
        addStudent(student);
    }
}

void StudentsTableModel::removeSelectedStudents(QModelIndexList rows)
{
    qSort(rows.begin(), rows.end(), qGreater<QModelIndex>());

    for(int i = 0; i < rows.size(); i++)
    {
        int row = rows.at(i).row();

        beginRemoveRows(QModelIndex(), row, row);
        m_listOfStudents.removeAt(row);
        endRemoveRows();
    }
}

void StudentsTableModel::removeAllStudents()
{
    if (m_listOfStudents.size() > 0)
    {
        beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
        m_listOfStudents.clear();
        endRemoveRows();
    }
}

void StudentsTableModel::saveFile(QString aFilePath)
{
    FileController::saveFile(aFilePath, m_listOfStudents);
}

void StudentsTableModel::open(QList<Student*> students)
{
    if (students.size() > 0)
    {
        removeAllStudents();
        addStudents(students);
    }
}

void StudentsTableModel::setListOfStudents(QList<Student *> listOfStudents)
{
    m_listOfStudents = listOfStudents;
}
