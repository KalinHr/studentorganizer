#ifndef STUDENT_H
#define STUDENT_H

#include <QString>
#include <QVariant>


class Student
{
public:
    Student(const QString& avatar,
            const QString& firstName,
            const QString& surName,
            const QString& lastName,
            const QString& fN,
            const int& course);

    QVariant GetData(int column) const;

    QString avatar();
    QString firstName();
    QString surName();
    QString lastName();
    QString fN();
    int course();

private:
    QString m_avatar;
    QString m_firstName;
    QString m_surName;
    QString m_lastName;
    QString m_fN;
    int m_course;
};

#endif // STUDENT_H
