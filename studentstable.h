#ifndef STUDENTSTABLE_H
#define STUDENTSTABLE_H

#include "studentstablemodel.h"

#include <QWidget>
#include <QTableView>
#include <QLineEdit>
#include <QSortFilterProxyModel>

class StudentsTable : public QWidget
{
    Q_OBJECT

public:
    explicit StudentsTable( QWidget* parent = Q_NULLPTR );

    void addStudent(Student* student);
    void deleteStudent();
    void save();
    void open(QList<Student*> students);

public slots:
    void filter(QString text);

private:
    QLineEdit* _filter;
    StudentsTableModel* _model;
    QSortFilterProxyModel* _proxyModel;
    QTableView* _tableView;
};

#endif // MAINWINDOW_H
