#ifndef STUDENTTABLEMODEL_H
#define STUDENTTABLEMODEL_H

class Student;

#include <QAbstractTableModel>
#include <QString>

class StudentsTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum ColumnIndex
    {
        ColumnIndexAvatar,
        ColumnIndexFirstName,
        ColumnIndexSurname,
        ColumnIndexLastName,
        ColumnIndexFN,
        ColumnIndexCourse,
        ColumnSize
    };

public:
    StudentsTableModel(QObject *parent = Q_NULLPTR );
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation = Qt::Horizontal,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    void addStudent( Student* const aStudent );
    void addStudents( const QList< Student* >& aStudents );
    void removeSelectedStudents(QModelIndexList rows);
    void removeAllStudents();
    void saveFile(QString aFilePath);
    void open(QList<Student*> students);

    void setListOfStudents(QList< Student* > listOfStudents);

private:
    QList< Student* > m_listOfStudents;
};

#endif // STUDENTTABLEMODEL_H
