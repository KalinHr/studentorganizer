#include "filecontroller.h"

#include <QFileDialog>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QImageWriter>
#include <QMessageBox>
#include <QtCore>
#include <QImage>

const QString FileController::sAvatar = "avatar";
const QString FileController::sStudents = "students";
const QString FileController::sStudent = "student";
const QString FileController::sFirstName = "firstName";
const QString FileController::sSurname = "surname";
const QString FileController::sLastName = "lastName";
const QString FileController::sFN = "fN";
const QString FileController::sCourse = "course";

QList< Student* > FileController::openFile(QString aFile)
{
    QFile file(aFile);

    QList< Student* > students;
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(0, "Error!", "Error opening file");

        return students;
    }

    QXmlStreamReader xml;
    xml.setDevice(&file);

    QString avatar;
    QString firstName;
    QString surName;
    QString lastName;
    QString fN;
    int course;

    while (!xml.isEndDocument())
    {
        xml.readNext();

        if (xml.isStartElement())
        {
            QString element = xml.name().toString();
            if (element == sFirstName)
                firstName = xml.readElementText();
            else if (element == sSurname)
                surName = xml.readElementText();
            else if (element == sLastName)
                lastName = xml.readElementText();
            else if (element == sFN)
                fN = xml.readElementText();
            else if (element == sCourse)
                course = xml.readElementText().toInt();
        }
        else if (xml.isEndElement())
        {
            if (xml.name().toString() == sStudent)
            {
                avatar = aFile.section("/",0,-2) + "/" + fN + ".jpg";
                Student* student = new Student(avatar, firstName, surName, lastName, fN, course);
                students.push_back(student);
            }
        }
    }

    return students;
}

void FileController::saveFile(QString aFile, const QList< Student* >& aStudents )
{
    QDir().mkdir(aFile);

    QFile xmlFile(aFile + "/students.xml");
    if (!xmlFile.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(0, "Error!", "Error opening file");
    }

    QXmlStreamWriter xml;
    xml.setDevice(&xmlFile);

    xml.writeStartDocument();
    xml.writeStartElement(sStudents);

    if(aStudents.size() > 0)
    {
        for (int i = 0; i < aStudents.size(); i++)
        {
            QFile imdgeFile(aFile + "/" + aStudents[i]->GetData(4).toString() + ".jpg");
            if (!imdgeFile.open(QIODevice::WriteOnly))
            {
                QMessageBox::warning(0, "Error!", "Error opening file");
            }

            QImageWriter imageWriter;
            imageWriter.setDevice(&imdgeFile);
            imageWriter.write(QImage(aStudents[i]->GetData(0).toString()));


            xml.writeStartElement(sStudent);

            xml.writeStartElement(sFirstName);
            xml.writeCharacters (aStudents[i]->GetData(1).toString());
            xml.writeEndElement();

            xml.writeStartElement(sSurname);
            xml.writeCharacters (aStudents[i]->GetData(2).toString());
            xml.writeEndElement();

            xml.writeStartElement(sLastName);
            xml.writeCharacters (aStudents[i]->GetData(3).toString());
            xml.writeEndElement();

            xml.writeStartElement(sFN);
            xml.writeCharacters (aStudents[i]->GetData(4).toString());
            xml.writeEndElement();

            xml.writeStartElement(sCourse);
            xml.writeCharacters (aStudents[i]->GetData(5).toString());
            xml.writeEndElement();

            xml.writeEndElement();

            imageWriter.setFileName(aStudents[i]->GetData(4).toString());
        }
    }

    xml.writeEndElement();
    xml.writeEndDocument();
}
