#ifndef XMLSTUDENTSSTREAM_H
#define XMLSTUDENTSSTREAM_H

#include "student.h"

#include <QList>

class FileController
{
private:
    static const QString sAvatar;
    static const QString sStudents;
    static const QString sStudent;
    static const QString sFirstName;
    static const QString sSurname;
    static const QString sLastName;
    static const QString sFN;
    static const QString sCourse;

public:
    static QList< Student* > openFile(QString aFile);
    static void saveFile(QString aFile, const QList< Student* >& aStudents );
};

#endif // XMLSTUDENTSSTREAM_H
