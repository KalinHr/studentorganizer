#include "mainwindow.h"
#include "addstudentdialog.h"
#include "filecontroller.h"

#include <QSplitter>
#include <QQuickView>
#include <QDir>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    :QMainWindow(parent)
    , _studentsTable(new StudentsTable)
    , _studentsList(new StudentsList)
    , _addStudent(new QPushButton)
    , _deleteStudent(new QPushButton)
    , _save(new QPushButton)
    , _open(new QPushButton)
{
    QSplitter* splitter = new QSplitter(Qt::Horizontal);
    splitter->addWidget(_studentsTable);
    splitter->addWidget(QWidget::createWindowContainer(_studentsList));

    buttonStyle(_addStudent, "plus_icon.png");
    buttonStyle(_deleteStudent, "minus_icon.png");
    buttonStyle(_save, "save_icon.png");
    buttonStyle(_open, "folder_icon.png");

    connect(_addStudent, SIGNAL(clicked()),
            this, SLOT(addStudent()));
    connect(_deleteStudent, SIGNAL(clicked()),
            this, SLOT(deleteStudent()));
    connect(_save, SIGNAL(clicked()),
            this, SLOT(save()));
    connect(_open, SIGNAL(clicked()),
            this, SLOT(open()));

    QGridLayout* gLayout = new QGridLayout;
    QHBoxLayout* buttons = new QHBoxLayout();
    buttons->addWidget(_addStudent);
    buttons->addWidget(_deleteStudent);
    buttons->addWidget(_save);
    buttons->addWidget(_open);

    gLayout->addLayout(buttons, 0, 1, Qt::AlignRight);

    QVBoxLayout* const mainLayout = new QVBoxLayout();
    mainLayout->addWidget(splitter);
    mainLayout->addLayout(gLayout);

    QWidget *window = new QWidget();
    window->setLayout(mainLayout);

    setCentralWidget(window);
}

void MainWindow::buttonStyle(QPushButton *button, QString path)
{
    QDir directory(":/icons");

    button->setIcon(QIcon(directory.filePath(path)));
    button->setStyleSheet("border-style: none;");
    button->setIconSize(QSize(32,32));
}

void MainWindow::addStudent()
{
    AddStudentDialog addStudentDialog;
    if (addStudentDialog.exec())
    {
        _studentsTable->addStudent(addStudentDialog.student());
        _studentsList->addStudent(addStudentDialog.student());
    }
}

void MainWindow::deleteStudent()
{
    _studentsTable->deleteStudent();
}

void MainWindow::save()
{
    _studentsTable->save();
}

void MainWindow::open()
{
    //QString filter = "File Description (*.xml)";
    QString filePath = QFileDialog::getOpenFileName(this, "Open file", QDir::homePath());
    QList<Student*> students = FileController::openFile(filePath);

    _studentsTable->open(students);
    _studentsList->open(students);
}
