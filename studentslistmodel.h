#ifndef STUDENTSLISTMODEL_H
#define STUDENTSLISTMODEL_H

class Student;

#include <QAbstractListModel>

class StudentsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum StudentRoles {
        AvatarRole = Qt::UserRole + 1,
        FirstNameRole,
        SurnameRole,
        LastNameRole,
        FNRole,
        CourseRole
    };

public:
    StudentsListModel(QObject *parent = Q_NULLPTR);

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    void addStudent(Student* student);
    void addStudents( const QList< Student* >& aStudents );
    void removeAllStudents();
    void open(QList<Student*> students);
    void setListOfStudents(QList< Student* >* listOfStudents);

private:
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

private:
    QList< Student* >* m_listOfStudents;
};

#endif // STUDENTSLISTMODEL_H
