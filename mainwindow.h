#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "studentstable.h"
#include "studentslist.h"

#include <QMainWindow>
#include <QPushButton>


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = Q_NULLPTR);

private:
    void buttonStyle(QPushButton* button, QString path);

public slots:
    void addStudent();
    void deleteStudent();
    void save();
    void open();

private:
    StudentsTable* _studentsTable;
    StudentsList* _studentsList;
    QPushButton* _addStudent;
    QPushButton* _deleteStudent;
    QPushButton* _save;
    QPushButton* _open;
};

#endif // MAINWINDOW_H
