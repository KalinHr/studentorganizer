import QtQuick 2.5

ListView {
    id: mylistView
    x: 2
    y: 2
//    cellWidth: 252
//    cellHeight: 122
    spacing: 2

    Component {
        id: student

        Rectangle {
            id: rect
            width: 250
            height: 120
            radius: 8
            state: "NORMAL"

            border {
                width: 2
                color: "#993300"
            }

            Image {
                x: 8
                y: 8
                width: 70
                height: 90
                source: "file:///" + avatar
            }

            Text {
                x: 100
                y: 16
                text: firstName
            }
            Text {
                x: 100
                y: 35
                text: lastName
            }
            Text {
                x: 100
                y: 70
                text: "№: " + fN
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    myLoader.sourceComponent = studentInfo
                    rect.state = "CLICED"
                }
            }

            Loader {
                parent: mylistView
                id: myLoader
                width: 250
                height: 120

                Component {
                    id: studentInfo

                    Rectangle {
                        color: "#FF9966"
                        radius: 8

                        focus: true

                        border {
                            width: 2
                            color: "#993300"
                        }

                        Image {
                            anchors.horizontalCenter: parent.horizontalCenter
                            y: 8
                            width: 70
                            height: 90
                            source: "file:///" + avatar

                            PropertyAnimation on width { to: 130; duration: 1500}
                            PropertyAnimation on height { to: 150; duration: 1500}
                        }

                        Text {
                            anchors.horizontalCenter: parent.horizontalCenter
                            y: 100
                            text: "First name:   <b>" + firstName + "</b>"

                            PropertyAnimation on y { to: 180; duration: 1500}
                        }
                        Text {
                            anchors.horizontalCenter: parent.horizontalCenter
                            y: 100
                            text: "Sur name:     <b>" + surName + "</b>"

                            PropertyAnimation on y { to: 200; duration: 1500}
                        }
                        Text {
                            anchors.horizontalCenter: parent.horizontalCenter
                            y: 100
                            text: "Last name:    <b>" + lastName + "</b>"

                            PropertyAnimation on y { to: 220; duration: 1500}
                        }
                        Text {
                            anchors.horizontalCenter: parent.horizontalCenter
                            y: 100
                            text: "Course:       <b>" + course + "</b>"

                            PropertyAnimation on y { to: 240; duration: 1500}
                        }
                        Text {
                            anchors.horizontalCenter: parent.horizontalCenter
                            y: 100
                            text: "№:            <b>" + fN + "</b>"

                            PropertyAnimation on y { to: 260; duration: 1500}
                        }

                        PropertyAnimation on x { to: 255; duration: 1500}
                        PropertyAnimation on height { to: 300; duration: 1500}
                    }
                }
            }

            states: [
                State {
                    name: "NORMAL"
                    PropertyChanges { target: rect; color: "#FF9966"}
                },
                State {
                    name: "CLICED"
                    PropertyChanges { target: rect; color: "#CC6633"}
                }
            ]

            transitions: Transition {
                from: "NORMAL"; to: "CLICED"
                    ColorAnimation { target: rect; duration: 200 }
            }
        }
    }

    model: myModel
    delegate: student
}
