#include "studentstable.h"
#include "addstudentdialog.h"
#include "student.h"

#include <QTableView>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QHBoxLayout>
#include <QWidget>
#include <QGridLayout>
#include <QDir>
#include <QFileDialog>
#include <QtCore>
#include <QSize>

StudentsTable::StudentsTable( QWidget* parent )
    : QWidget( parent )
    , _filter(new QLineEdit)
    , _model(new StudentsTableModel)
    , _proxyModel(new QSortFilterProxyModel)
    , _tableView(new QTableView)
{
    _proxyModel->setSourceModel(_model);
    _tableView->setModel( _proxyModel );
    _tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    _tableView->verticalHeader()->hide();
    _tableView->setIconSize(QSize(30, 30));

    connect(_filter, SIGNAL(textChanged(QString)), this, SLOT(filter(QString)));

    QVBoxLayout* const mainLayout = new QVBoxLayout( this );
    mainLayout->addWidget(_filter);
    mainLayout->addWidget(_tableView);
}

void StudentsTable::addStudent(Student* student)
{
    _model->addStudent(student);
}

void StudentsTable::deleteStudent()
{
    QModelIndexList rows = _tableView->selectionModel()->selectedRows();
    _model->removeSelectedStudents(rows);
}

void StudentsTable::save()
{
    QString filePath = QFileDialog::getSaveFileName(this, "Save file", QDir::homePath());
    _model->saveFile(filePath);
}

void StudentsTable::open(QList<Student*> students)
{
    _model->open(students);
}

void StudentsTable::filter(QString text)
{
    //_proxyModel->setFilterKeyColumn(1);
    _proxyModel->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive, QRegExp::FixedString));
}
