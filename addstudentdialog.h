#ifndef ADDSTUDENT_H
#define ADDSTUDENT_H

#include "student.h"

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class AddStudentDialog : public QDialog
{
    Q_OBJECT

public:
    AddStudentDialog(QWidget* parent = Q_NULLPTR);

    Student* student();

public slots:
    void selectAvatar();
    void checkInformation();

private:
    QLineEdit* m_firstNameText;
    QLineEdit* m_surnameText;
    QLineEdit* m_lastNameText;
    QLineEdit* m_fNText;
    QLineEdit* m_courseText;

    QLabel* m_avatarLabel;
    QLabel* m_firstNameLabel;
    QLabel* m_surnameLabel;
    QLabel* m_lastNameLabel;
    QLabel* m_fNLabel;
    QLabel* m_courseLabel;

    QPushButton* m_selectAvatar;
    QString m_avatar;

    QPushButton* m_okButton;
    QPushButton* m_cancelButton;
};

#endif // ADDSTUDENT_H
