#include "studentslistmodel.h"
#include "student.h"

StudentsListModel::StudentsListModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_listOfStudents(new QList<Student*>)
{
}

void StudentsListModel::addStudent(Student *student)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_listOfStudents->append(student);
    endInsertRows();
}

void StudentsListModel::addStudents(const QList<Student *> &aStudents)
{
    foreach(Student* student, aStudents)
    {
        addStudent(student);
    }
}

void StudentsListModel::removeAllStudents()
{
    if (m_listOfStudents->size() > 0)
    {
        beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
        m_listOfStudents->clear();
        endRemoveRows();
    }
}

void StudentsListModel::open(QList<Student *> students)
{
    if (students.size() > 0)
    {
        removeAllStudents();
        addStudents(students);
    }
}

QVariant StudentsListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    if (index.row() < 0 || index.row() >= m_listOfStudents->count())
    {
        return QVariant();
    }

    Student* student = m_listOfStudents->at(index.row());
    if (role == AvatarRole)
    {
        return student->avatar();
    }
    else if (role == FirstNameRole)
    {
        return student->firstName();
    }
    else if (role == SurnameRole)
    {
        return student->surName();
    }
    else if (role == LastNameRole)
    {
        return student->lastName();
    }
    else if (role == FNRole)
    {
        return student->fN();
    }
    else if (role == CourseRole)
    {
        return student->course();
    }

    return QVariant();
}

int StudentsListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_listOfStudents->count();
}

QHash<int, QByteArray> StudentsListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[AvatarRole] = "avatar";
    roles[FirstNameRole] = "firstName";
    roles[SurnameRole] = "surName";
    roles[LastNameRole] = "lastName";
    roles[FNRole] = "fN";
    roles[CourseRole] = "course";

    return roles;
}

void StudentsListModel::setListOfStudents(QList<Student *>* listOfStudents)
{
    m_listOfStudents = listOfStudents;
}
